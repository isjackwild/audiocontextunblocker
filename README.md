Audio Context Unblocker
=========

A utility class which detects if the AudioContext has been automatically suspended (Thanks, Google!), and displays a Google Chrome style User Interface to alert the user and allow them to easily unblock the audio.

## Installation

  `npm install audio-context-unblocker`

## Usage

    import AudioContextUnblocker from 'audio-context-unblocker'

    const myAudioContext = new AudioContext() // your existing audio context.
    const audioContextUnblocker = new AudioContextUnblocker(myAudioContext);
  
  
  If the AudioContextUnblocker detects that the passed context has been automatically blocked, a UI will be shown to unblock.

  ![Audio Context Unblocker](http://sketchbook.isjackwild.com/audio-ctx-unblocker.png)