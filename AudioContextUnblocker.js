const style = `
.actx-unblocker * {
	user-select: none;
	-webkit-user-select: none;
}

.actx-unblocker {
	position: fixed;
	top: 0px;
	left: 92px;
	background: #fff;
	border-radius: 3px;
	z-index: 9999999;
	box-shadow: 0 10px 30px rgba(0, 0, 0, 0.15);
	font-family: sans-serif;
	text-rendering: optimizeLegibility;
	-webkit-text-size-adjust: 100%;
	-webkit-font-smoothing: antialiased;
	-moz-osx-font-smoothing: grayscale;
	font-feature-settings: "liga", "kern";
	padding: 19px 14px 16px 16px;
	height: 127px;
	width: 320px;
	border: 1px solid #ddd;
	user-select: none;
	-webkit-user-select: none;
}

.actx-unblocker__inner {
	position: relative;
	height: 100%;
	width: 100%;
}

.actx-unblocker__title {
	display: block;
	margin-bottom: 15px;
	line-height: 1em;
	color: #212121;
	font-size: 16px;
}

.actx-unblocker__subtitle {
	display: block;
	line-height: 1em;
	color: #212121;
	font-size: 12px;
	letter-spacing: 0.005em;
}

.actx-unblocker__buttons {
	width: 100%;
	position: absolute;
	bottom: 0;
	left: 0;
	height: 28px;
	text-align: right;
}

.actx-unblocker__button {
	height: 29px;
	width: 66px;
	background: #fff;
	appearance: none;
	-webkit-appearance: none;
	-moz-appearance: none;
	border-radius: 3px;
	outline: none;
	border: 1px solid #ccc;
	font-weight: bold;
	font-size: 12px;
	color: #757575;
}

.actx-unblocker__close {
	position: absolute;
	top: -9px;
	right: -4px;
	width: 13px;
	height: 13px;
	cursor: pointer;
}

.actx-unblocker__close:before,
.actx-unblocker__close:after {
	position: absolute;
	top: 0;
	left: 50%;
	margin-left: -1px;
	width: 2px;
	height: 100%;
	content: '';
	background: #5a5a5a;
	transform-origin: center;
}

.actx-unblocker__close:before {
	transform: rotate(-45deg);
}


.actx-unblocker__close:after {
	transform: rotate(45deg);	
}
`


class AudioContextUnblocker {
	constructor(context) {
		this.context = context || new (window.AudioContext || window.webkitAudioContext)();
		this.el = null;

		this.unblock = this.unblock.bind(this);
		this.close = this.close.bind(this);
		this.addEventListeners = this.addEventListeners.bind(this);
		
		if (this.context) this.checkContextState();
	}

	checkContextState() {
		if (this.context.state === 'suspended')
			this.render();
	}

	render() {
		this.stylesheet = document.createElement('style');
		this.stylesheet.innerHTML = style;
		document.body.appendChild(this.stylesheet);

		this.el = document.createElement('div');
		this.el.className = "actx-unblocker";
		this.el.innerHTML = `
			<div class="actx-unblocker__inner">
				<span class="actx-unblocker__title">${window.location.hostname} wants to</span>
				<span class="actx-unblocker__subtitle">Play audio</span>
				<div class="actx-unblocker__buttons">
					<button class="actx-unblocker__button actx-unblocker__button--allow">Allow</button>
				</div>
				<div class="actx-unblocker__close"></div>
			</div>`
		document.body.appendChild(this.el);
		setTimeout(this.addEventListeners, 0);
	}

	addEventListeners() {
		this.allowButton = this.el.querySelector('.actx-unblocker__button--allow');
		this.allowButton.addEventListener('click', this.unblock);

		this.closeButton = this.el.querySelector('.actx-unblocker__close');
		this.closeButton.addEventListener('click', this.close);
	}

	close() {
		document.body.removeChild(this.el);
		document.body.removeChild(this.stylesheet);
		this.allowButton.removeEventListener('click', this.unblock);
		this.closeButton.removeEventListener('click', this.close);
		this.el = undefined;
	}

	unblock() {
		this.context.resume().then(() => console.log('audio playback resumed'));
		this.close();
	}
}

export default AudioContextUnblocker;

